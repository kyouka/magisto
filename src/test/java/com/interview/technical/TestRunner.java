package com.interview.technical;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/feature-files",
        glue = "com/interview/technical/stepdef"
)
public class TestRunner {
    protected static WebDriver driver;

    @AfterClass
    public static void tearDown(){
        driver.quit();
    }
}