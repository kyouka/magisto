package com.interview.technical.stepdef;

import com.interview.technical.TestRunner;
import com.interview.technical.stepdef.model.SignUpPage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class GetStarted extends TestRunner {
    private SignUpPage signUpPage;

    public GetStarted(BackgroundSteps backgroundSteps) {
        this.signUpPage = backgroundSteps.getSignUpPage();
    }

    @When("the submit button is clicked")
    public void theSubmitButtonIsClicked() {
        signUpPage.clickSubmitButton();
    }

    @Then("the {string} error message of {string} should be shown")
    public void theGivenErrorMessageOfGivenAttributeShouldBeShown(String message, String attribute) {
        signUpPage.setErrorLabelWithGivenAttribute(attribute);
        Assert.assertEquals(message, signUpPage.getErrorLabelText());
    }

    @When("the {string} is filled in with {string}")
    public void theFieldIsFilledInWithParameter(String field, String parameter) {
        signUpPage.fillFieldWithContent(field, parameter);
    }

    @When("the show or hide password button is clicked")
    public void theShowHidePasswordButtonIsClicked() {
        signUpPage.clickShowHidePasswordButton();
    }

    @Then("the password text should be shown")
    public void thePasswordTextShouldBeShown() {
        Assert.assertEquals("text", signUpPage.getPasswordFieldTypeAttribute());
    }

    @Then("the password text should not be shown")
    public void thePasswordTextShouldNotBeShown() {
        Assert.assertEquals("password", signUpPage.getPasswordFieldTypeAttribute());
    }

    @Then("the {string} error message should be shown")
    public void theInvalidEmailPasswordErrorMessageIsShown(String message) {
        Assert.assertEquals(message, signUpPage.getGlobalErrorLabel().getText());
    }

    @Given("the forgot password link is clicked")
    public void theForgotPasswordLinkIsClicked() {
        signUpPage.clickForgotPasswordText();
        signUpPage = new SignUpPage(driver);
    }
}
