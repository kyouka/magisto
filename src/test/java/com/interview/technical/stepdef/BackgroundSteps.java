package com.interview.technical.stepdef;

import com.interview.technical.TestRunner;
import com.interview.technical.stepdef.model.HomePage;
import com.interview.technical.stepdef.model.SignUpPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BackgroundSteps extends TestRunner {
    private static final int DRIVER_WIDTH = 1200;
    private static final int DRIVER_HEIGHT = 900;
    private static final int IMPLICIT_WAIT_TIME = 5;
    private static final int WAIT_TIME = 10;

    private HomePage homePage;
    private SignUpPage signUpPage;

    static {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(DRIVER_WIDTH, DRIVER_HEIGHT));
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS).pageLoadTimeout(WAIT_TIME, TimeUnit.SECONDS);
    }

    @Given("the home page is opened")
    public void theHomePageIsOpened() {
        homePage = new HomePage(driver);
        homePage.open();
    }

    @And("the accept cookies button is clicked")
    public void theAcceptCookiesButtonIsClicked() {
        homePage.clickAcceptCookiesButton();
    }

    @Given("the Get Started button is clicked")
    public void theGetStartedButtonIsClicked() {
        homePage.clickGetStartedButton();
        signUpPage = new SignUpPage(driver);
    }

    @And("the Sign In button is clicked")
    public void theSignInButtonIsClicked() {
        homePage.clickSignInButton();
        signUpPage = new SignUpPage(driver);
    }

    public HomePage getHomePage() {
        return homePage;
    }

    public SignUpPage getSignUpPage() {
        return signUpPage;
    }
}
