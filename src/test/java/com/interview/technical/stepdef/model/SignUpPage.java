package com.interview.technical.stepdef.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

public class SignUpPage {
    private static final int WAIT_TIME = 5;

    private WebDriver driver;

    @FindBy(css = "button.full_width")
    private WebElement submitButton;

    @FindBy(className = "eye-icon")
    private WebElement showHidePasswordButton;

    @FindBy(css = "input[name=password]")
    private WebElement passwordField;

    @FindBy(className = "global_error")
    private WebElement globalErrorLabel;

    @FindBy(css = ".forgot > a:nth-child(1)")
    private WebElement forgotPasswordText;

    private WebElement errorLabel;

    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void fillFieldWithContent(String field, String content) {
        new WebDriverWait(driver, WAIT_TIME).until(elementToBeClickable(By.cssSelector(String.format("input[ng-model=\"%s\"]", field)))).sendKeys(content);
    }

    public void setErrorLabelWithGivenAttribute(String attribute) {
        this.errorLabel = driver.findElement(By.cssSelector(String.format("span[ng-bind-html=\"%s\"]", attribute)));
    }

    public String getPasswordFieldTypeAttribute() {
        return getPasswordField().getAttribute("type");
    }

    public String getErrorLabelText() {
        return getErrorLabel().getText();
    }

    public void clickSubmitButton() {
        getSubmitButton().click();
    }

    public void clickShowHidePasswordButton() {
        getShowHidePasswordButton().click();
    }

    public void clickForgotPasswordText() {
        getForgotPasswordText().click();
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public WebElement getShowHidePasswordButton() {
        return showHidePasswordButton;
    }

    public WebElement getPasswordField() {
        return passwordField;
    }

    public WebElement getGlobalErrorLabel() {
        return globalErrorLabel;
    }

    public WebElement getForgotPasswordText() {
        return forgotPasswordText;
    }

    public WebElement getErrorLabel() {
        return errorLabel;
    }
}
