package com.interview.technical.stepdef.model;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class HomePage {
    private static final String PAGE_URL = "https://vimeo.com/cameo";

    private WebDriver driver;

    @FindBy(css = "a.cookies_accept:nth-child(2) > button:nth-child(1)")
    private WebElement acceptCookiesDesktopButton;

    @FindBy(css = "a.cookies_accept:nth-child(1) > button:nth-child(1)")
    private WebElement acceptCookiesMobileButton;

    @FindBy(css = "button.large")
    private WebElement getStartedButton;

    @FindBy(css = ".dark")
    private WebElement signInButton;

    @FindBy(id = "hamburger_icon")
    private WebElement hamburgerIcon;

    @FindBy(className = "top_menu")
    private WebElement topMenu;

    @FindBy(className = "logos_wrap")
    private WebElement appStoreLogosWrapper;

    @FindBy(className = "cookies_consent_section")
    private WebElement cookiesSection;

    @FindBy(css = ".cookies_consent_action > a:nth-child(1)")
    private WebElement cookiesLearnMoreLink;

    @FindBy(className = "sm-hide")
    private List<WebElement> desktopOnlyElementsInFooter;

    private List<WebElement> onlyBigElements;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get(PAGE_URL);
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();
    }

    public void setOnlyBigElements() {
        this.onlyBigElements = driver.findElements(By.className("only_big"))
                .stream()
                .filter(element -> !element.getTagName().equals("br"))
                .collect(Collectors.toList());
    }

    public void clickAcceptCookiesButton() {
        getAcceptCookiesDesktopButton().click();
    }

    public void clickGetStartedButton() {
        getGetStartedButton().click();
    }

    public void clickSignInButton() {
        getSignInButton().click();
    }

    public void clickCookiesLearnMoreLink() {
        getCookiesLearnMoreLink().click();
    }

    public WebElement getAcceptCookiesDesktopButton() {
        return acceptCookiesDesktopButton;
    }

    public WebElement getAcceptCookiesMobileButton() {
        return acceptCookiesMobileButton;
    }

    public WebElement getGetStartedButton() {
        return getStartedButton;
    }

    public WebElement getSignInButton() {
        return signInButton;
    }

    public WebElement getHamburgerIcon() {
        return hamburgerIcon;
    }

    public WebElement getTopMenu() {
        return topMenu;
    }

    public WebElement getAppStoreLogosWrapper() {
        return appStoreLogosWrapper;
    }

    public WebElement getCookiesSection() {
        return cookiesSection;
    }

    public WebElement getCookiesLearnMoreLink() {
        return cookiesLearnMoreLink;
    }

    public List<WebElement> getOnlyBigElements() {
        return onlyBigElements;
    }

    public List<WebElement> getDesktopOnlyElementsInFooter() {
        return desktopOnlyElementsInFooter;
    }
}
