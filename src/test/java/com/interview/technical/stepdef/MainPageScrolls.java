package com.interview.technical.stepdef;

import com.interview.technical.TestRunner;
import com.interview.technical.stepdef.model.HomePage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Dimension;

public class MainPageScrolls extends TestRunner {
    private HomePage homePage;

    public MainPageScrolls(BackgroundSteps backgroundSteps) {
        this.homePage = backgroundSteps.getHomePage();
    }

    @When("the viewport width is set to {int} px")
    public void theViewportWidthIsSetToGivenPx(int width) {
        int currentHeight = driver.manage().window().getSize().getHeight();
        Dimension dimension = new Dimension(width, currentHeight);
        driver.manage().window().setSize(dimension);
        homePage.setOnlyBigElements();
    }

    @Then("desktop only elements should be shown")
    public void onlyDesktopElementsShouldBeShown() {
        homePage.getOnlyBigElements().forEach(
                element -> Assert.assertTrue(element.isDisplayed())
        );
        homePage.getDesktopOnlyElementsInFooter().forEach(
                element -> Assert.assertTrue(element.isDisplayed())
        );
        Assert.assertTrue(homePage.getTopMenu().isDisplayed());
        Assert.assertTrue(homePage.getCookiesSection().isDisplayed());
    }

    @And("desktop only elements should not be shown")
    public void onlyDesktopElementsShouldNotBeShown() {
        homePage.getOnlyBigElements().forEach(
                element -> Assert.assertFalse(element.isDisplayed())
        );
        homePage.getDesktopOnlyElementsInFooter().forEach(
                element -> Assert.assertFalse(element.isDisplayed())
        );
        Assert.assertFalse(homePage.getTopMenu().isDisplayed());
        Assert.assertFalse(homePage.getCookiesSection().isDisplayed());
    }

    @Then("mobile elements should be shown")
    public void mobileElementsShouldBeShown() {
        Assert.assertTrue(homePage.getHamburgerIcon().isDisplayed());
        Assert.assertTrue(homePage.getAppStoreLogosWrapper().isDisplayed());
    }

    @And("mobile elements should not be shown")
    public void mobileElementsShouldNotBeShown() {
        Assert.assertFalse(homePage.getHamburgerIcon().isDisplayed());
        Assert.assertFalse(homePage.getAppStoreLogosWrapper().isDisplayed());
    }

    @When("the hamburger menu is clicked")
    public void theHamburgerMenuIsClicked() {
        homePage.getHamburgerIcon().click();
    }

    @Then("the top menu should be shown")
    public void theTopMenuShouldBeShown() {
        Assert.assertTrue(homePage.getTopMenu().isDisplayed());
    }

    @Then("the top menu should not be shown")
    public void theTopMenuShouldNotBeShown() {
        Assert.assertFalse(homePage.getTopMenu().isDisplayed());
    }

    @When("the learn more link is clicked")
    public void theLearnMoreLinkIsClicked() {
        homePage.clickCookiesLearnMoreLink();
    }

    @Then("the accept cookies button should be shown")
    public void theAcceptCookiesButtonShouldBeShown() {
        Assert.assertTrue(homePage.getAcceptCookiesMobileButton().isDisplayed());
    }
}
