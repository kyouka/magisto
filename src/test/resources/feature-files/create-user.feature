Feature: Create a new user

  Background:
    Given the home page is opened
    And the accept cookies button is clicked
    And the Get Started button is clicked

  Scenario: Check required fields
    When the submit button is clicked
    Then the 'Please enter your full name' error message of 'signup_form.errors.name' should be shown
    Then the 'Please enter your email address' error message of 'signup_form.errors.mail' should be shown
    Then the 'Please enter a password' error message of 'signup_form.errors.password' should be shown

  Scenario Outline: Check the fields with invalid parameters
    When the '<field>' is filled in with '<parameter>'
    And the submit button is clicked
    Then the '<error-message>' error message of '<error-field>' should be shown
    Examples:
      | field                | parameter | error-message                                                                            | error-field                 |
      | signup_form.name     | a         | Please enter your full name                                                              | signup_form.errors.name     |
      | signup_form.email    | asd       | The email address entered is invalid                                                     | signup_form.errors.mail     |
      | signup_form.email    | asd@asd   | The email address entered is invalid                                                     | signup_form.errors.mail     |
      | signup_form.email    | asd@asd.  | The email address entered is invalid                                                     | signup_form.errors.mail     |
      | signup_form.email    | @asd.asd  | The email address entered is invalid                                                     | signup_form.errors.mail     |
      | signup_form.password | asd       | Your password must be at least 8 characters long and contain 4 or more unique characters | signup_form.errors.password |
      | signup_form.password | aaaaaaaa  | Your password must be at least 8 characters long and contain 4 or more unique characters | signup_form.errors.password |
      | signup_form.password | asdf      | Your password must be at least 8 characters long and contain 4 or more unique characters | signup_form.errors.password |

  Scenario: Check the fields with valid but existing email
    When the 'signup_form.name' is filled in with 'asd'
    And the 'signup_form.email' is filled in with 'asd@fgh.jkl'
    And the 'signup_form.password' is filled in with 'asdfghjkl'
    And the submit button is clicked
    Then the 'Email is already in use.' error message of 'signup_form.errors.mail' should be shown
    And the 'The email asd@fgh.jkl is already in use' error message should be shown

  Scenario: Check the show/hide password button if it is enabled
    Given the 'signup_form.password' is filled in with 'asdf'
    When the show or hide password button is clicked
    Then the password text should be shown

  Scenario: Check the show/hide password button if it is disabled
    Given the 'signup_form.password' is filled in with 'asdf'
    And the show or hide password button is clicked
    When the show or hide password button is clicked
    Then the password text should not be shown
