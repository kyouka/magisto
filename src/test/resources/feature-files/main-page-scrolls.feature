Feature: Scrolls on the main page

  Background:
    Given the home page is opened

  Scenario: Check elements in desktop mode
    When the viewport width is set to 1200 px
    Then desktop only elements should be shown
    And mobile elements should not be shown

  Scenario: Check elements in mobile mode
    When the viewport width is set to 600 px
    Then mobile elements should be shown
    And desktop only elements should not be shown

  Scenario: Check hamburger menu in mobile mode
    Given the viewport width is set to 600 px
    When the hamburger menu is clicked
    Then the top menu should be shown

  Scenario: Check the closing of the hamburger menu in mobile mode
    Given the viewport width is set to 600 px
    And the hamburger menu is clicked
    When the hamburger menu is clicked
    Then the top menu should not be shown

  Scenario: Check the cookie disclaimer in mobile mode
    Given the viewport width is set to 600 px
    When the learn more link is clicked
    Then the accept cookies button should be shown