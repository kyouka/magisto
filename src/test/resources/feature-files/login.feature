Feature: Login

  Background:
    Given the home page is opened
    And the accept cookies button is clicked
    And the Sign In button is clicked

  Scenario: Check required fields
    When the submit button is clicked
    Then the 'Please enter a valid Email address' error message of 'login_form.errors.email' should be shown
    And the 'Please enter a valid password' error message of 'login_form.errors.password' should be shown

  Scenario Outline: Check the email field with invalid emails
    When the 'login_form.email' is filled in with '<email>'
    And the submit button is clicked
    Then the 'The Email address you entered is invalid' error message of 'login_form.errors.email' should be shown
    Examples:
      | email    |
      | asd      |
      | asd@asd  |
      | asd@asd. |
      | @asd.asd |

  Scenario: Check existing email with invalid password
    When the 'login_form.email' is filled in with 'asd@fgh.jkl'
    And the 'login_form.password' is filled in with 'asd'
    And the submit button is clicked
    Then the 'Invalid email / password' error message should be shown

  Scenario: Check the show/hide password button if it is enabled
    Given the 'login_form.password' is filled in with 'asdf'
    When the show or hide password button is clicked
    Then the password text should be shown

  Scenario: Check the show/hide password button if it is disabled
    Given the 'login_form.password' is filled in with 'asdf'
    And the show or hide password button is clicked
    When the show or hide password button is clicked
    Then the password text should not be shown

  Scenario Outline: Check forgotten password with invalid emails
    Given the forgot password link is clicked
    And the 'forgot_form.email' is filled in with '<email>'
    When the submit button is clicked
    Then the 'The Email address you entered is invalid' error message of 'forgot_form.error' should be shown
    Examples:
      | email    |
      | asd      |
      | asd@asd  |
      | asd@asd. |
      | @asd.asd |